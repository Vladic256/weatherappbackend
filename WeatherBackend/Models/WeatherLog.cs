﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeatherBackend.Models
{
    public class WeatherLog
    {
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// Date of when the log was created
        /// </summary>
        public DateTime TimeStamp { get; private set; }
        /// <summary>
        /// Zipcode of the requested weather location
        /// </summary>
        public string ZipCode { get; private set; }
        /// <summary>
        /// Temperature of the weather during the request.
        /// </summary>
        public string Temperature { get; private set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="requestDate"> time of request</param>
        /// <param name="zipcode">zipcode of locatioin weather</param>
        /// <param name="temp">Temp of the weather for that zipcode</param>
        public WeatherLog(DateTime requestDate, string zipcode, string temperature)
        {
            this.TimeStamp = requestDate;
            this.ZipCode = zipcode;
            this.Temperature = temperature;
        }
    }
}