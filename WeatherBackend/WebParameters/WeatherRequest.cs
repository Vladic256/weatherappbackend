﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherBackend.WebParameters
{
    public class WeatherRequest
    {
        public string Temperature { get; set; }
        public string ZipCode { get; set; }
    }
}