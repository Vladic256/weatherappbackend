﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using WeatherBackend.Infrastruture;
using WeatherBackend.Models;
using WeatherBackend.WebParameters;
namespace WeatherBackend.Controllers
{
    public class WeatherController : ApiController
    {
        /// <summary>
        /// Retrieve weather of the day by the zipcode.
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        [Route("weather"), HttpPost]
        public IHttpActionResult GetWeatherDetail(WeatherRequest details)
        {
            if (details == null)
                throw new Exception("Please provide input details");

            using (var db = new WeatherDbContext())
            {

                try
                {
                    //Log the result into our local db.
                    var temperature = details.Temperature;
                    var TimeStamp = DateTime.Now;
                    var NewWeatherLog = new WeatherLog(TimeStamp, details.ZipCode, temperature);

                    db.Weather.Add(NewWeatherLog);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    //Todo: Handle exception 
                }
            }
            return Ok();
        }
    }
}
