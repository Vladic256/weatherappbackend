﻿using System.Data.Entity;
using WeatherBackend.Models;

namespace WeatherBackend.Infrastruture
{
    public class WeatherDbContext : DbContext
    {
        public WeatherDbContext() : base("name=WeatherServerDatabase")
        {

        }

        public DbSet<WeatherLog> Weather { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new WeatherLogMapping());
        }
    }
}