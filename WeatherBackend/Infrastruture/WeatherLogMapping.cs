﻿using System.Data.Entity.ModelConfiguration;
using WeatherBackend.Models;

namespace WeatherBackend.Infrastruture
{
    public class WeatherLogMapping : EntityTypeConfiguration<WeatherLog>
    {
        public WeatherLogMapping()
        {
            ToTable("WeatherLogTable");

            HasKey(p => p.Id);

            Property(p => p.TimeStamp).HasColumnName("TimeStamp");
            Property(p => p.Temperature).HasColumnName("Temperature");
            Property(p => p.ZipCode).HasColumnName("ZipCode");
        }
    }
}